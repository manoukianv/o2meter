// Inclusion de la librairie LiquidCrystal
#include <LiquidCrystal.h>
#include <Adafruit_ADS1015.h>
#include <Wire.h>
#include <RunningAverage.h>

#define NB_SAMPLE_QUALIBRATION 100
#define MAX_CALIBRATION_DELTA 2

#define O2_CALIBRATE_GAZ  20.95

#define ADC_GAIN  0.0078125

byte p20[8] = {
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
};
byte p40[8] = {
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
};
byte p60[8] = {
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
};
byte p80[8] = {
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
};
byte p100[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};

// Initialisation de l'écran LCD avec les numéros des broches reliées à l'interface
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
Adafruit_ADS1115 ads1115;
RunningAverage sensorVoltageAverage(10);
float o2VoltageInAir;

void setup()
{
  // Initialisation de l'écran LCD
  lcd.begin(16, 2);

  // Affichage du message d'accueil
  lcd.print("O2Meter     v1.1");

  // Init du convertisseur ADC
  lcd.setCursor(0, 1);
  lcd.print("ADC...");
  ads1115.begin();
  ads1115.setGain(GAIN_SIXTEEN);
  lcd.print(" Ok");
  delay(2000);
  
  // Init LCD
  //Make progress characters
  lcd.createChar(0, p20);
  lcd.createChar(1, p40);
  lcd.createChar(2, p60);
  lcd.createChar(3, p80);
  lcd.createChar(4, p100);

  // Init des variable : le RA
  sensorVoltageAverage.clear();
  lcd.clear();

  // calibrate the sensor
  o2VoltageInAir = getO2VoltageInAir();
}

void loop()
{
  // store de voltage in RA
  sensorVoltageAverage.addValue(ads1115.readADC_SingleEnded(0) * ADC_GAIN);
  
  float O2FractionInGaz =  O2_CALIBRATE_GAZ * sensorVoltageAverage.getAverage() / o2VoltageInAir;
  displayPPO2(sensorVoltageAverage.getAverage(),O2FractionInGaz);

  delay(500);
}

void displayPPO2(float voltage, float O2FractionInGaz) {
  lcd.setCursor(0, 0);
  lcd.print("O2            mV");
  lcd.setCursor(10, 0);
  lcd.print(voltage, 1);
  

  lcd.setCursor(0, 1);
  lcd.print("==>        % <==");
  lcd.setCursor(5, 1);
  lcd.print(O2FractionInGaz, 2);
  
}

float getO2VoltageInAir() {
  byte compteur=0;
  
  lcd.setCursor(0, 0);
  lcd.print("Calibrate ");
  
  // If currently not calibrate and not enough sample to calibrate : do the operation
  while (compteur < NB_SAMPLE_QUALIBRATION) {
    // get the current read voltage on the cell
    float sensorVoltage_OLD = sensorVoltageAverage.getAverage();
    // store de voltage in RA
    sensorVoltageAverage.addValue(ads1115.readADC_SingleEnded(0) * ADC_GAIN);
    
    // compute the delta calibration
    float delta_milli = 10000 * abs(sensorVoltageAverage.getAverage() - sensorVoltage_OLD) / sensorVoltage_OLD;

    // display the current calibration data
    lcd.setCursor(10, 0);
    lcd.print(sensorVoltageAverage.getAverage(), 3);
    lcd.print("mV");
    
    // if delta is less than the max, increment the compteur and show progress bar
    if (delta_milli < MAX_CALIBRATION_DELTA) {

      lcd.setCursor(0, 1); //Move the cursor to this location
      //Display the complete char in the first time
      byte nbCharPlein = compteur*80/500;
      for (int i = 0; i<nbCharPlein; i++) {
        lcd.write(4);
      }
      // display the subchar that show a slow progress
      //Compute the number of item display
      byte nbCompteurAffiche = (byte) nbCharPlein*5/0.8;
      byte charNumber = (compteur-nbCompteurAffiche)*4/6;
      lcd.write(charNumber);        //update progress bar
      
      compteur += 1;

    }
    delay(200);
  } 
  
  return sensorVoltageAverage.getAverage();
  
}
